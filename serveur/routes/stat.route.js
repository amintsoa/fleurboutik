module.exports = function (router) {
  let statRoute = router.route('/stat');
  statRoute.get((req, res, next) => {
    req.getConnection(function (err, conn) {
      let requete = "SELECT  count(*) AS total, sum(if(couleur = 1, 1, 0)) AS noir, sum(if(couleur = 2, 1, 0)) AS blanc FROM fleurs";
      conn.query(requete, (err, rows) => {
        if (err) {
          console.log(err);
          return next("Mysql error ...");
        }
        res.send(rows[0]);
      })
    });
  });
}
