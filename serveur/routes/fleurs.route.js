module.exports = function (router) {
  let flsRoute = router.route('/fleur');

// Liste des fleurs
  flsRoute.get((req, res, next) => {
    // TODO à affecter à un Controlleur
    req.getConnection(function (err, conn) {
      if (err) return next("PB Connection");

      let requete = "SELECT fleurs.id AS id, fleurs.nom AS nom, fleurs.prix AS prix, " +
        "fleurs.couleur AS couleur, couleur.nom AS nomCouleur " +
        "FROM fleurs JOIN couleur ON fleurs.couleur = couleur.id ";

      conn.query(requete, (err, rows) => {
        if (err) {
          console.log(err);
          return next("Mysql error ...");
        }
        res.send(rows);
      });
    });
  });

  flsRoute.post((req, res, next) => {
    req.assert('nom', 'Le nom est obligatoire').notEmpty();
    req.assert('prix', 'Le prix est obligatoire.').notEmpty();
    req.assert('prix', 'Le prix doit être un nombre.').isDecimal();

    let errors = req.validationErrors();
    if (errors) {
      res.status(500).send(errors);
      return;
    }

    let data = {
      nom: req.body.nom,
      prix: req.body.prix,
      couleur: req.body.couleur
    };

    // TODO à affecter à un Controlleur
    req.getConnection(function (err, conn) {
      if (err) return next("PB Connection");
      conn.query("INSERT INTO fleurs set ? ", data, (err, rows) => {
        if (err) {
          console.log(err);
          return next("Mysql error ...");
        }
        res.send(rows);
      });
    });
  });

  let flRoute = router.route('/fleur/:fl_id');

// Fiche fleur
  flRoute.get((req, res, next) => {
    let fl_id = req.params.fl_id;

    // TODO à affecter à un Controlleur
    req.getConnection(function (err, conn) {
      if (err) return next("PB Connection");
      let requete = "SELECT fleurs.id AS id, fleurs.nom AS nom, fleurs.prix AS prix, " +
        "fleurs.couleur AS couleur, couleur.nom AS nomCouleur " +
        "FROM fleurs JOIN couleur ON fleurs.couleur = couleur.id " +
        "WHERE fleurs.id = ?";

      conn.query(requete, [fl_id], (err, rows) => {
        if (err) {
          console.log(err);
          return next("Mysql error ...");
        }
        res.send(rows[0]);
      });
    });
  });

// Update
  flRoute.put((req, res, next) => {
    let fl_id = req.params.fl_id;

    req.assert('nom', 'Le nom est obligatoire').notEmpty();
    req.assert('prix', 'Le prix est obligatoire.').notEmpty();
    req.assert('prix', 'Le prix doit être un nombre.').isDecimal();

    let errors = req.validationErrors();
    if (errors) {
      res.status(500).send(errors);
      return;
    }

    let data = {
      nom: req.body.nom,
      prix: req.body.prix,
      couleur: req.body.couleur
    };

    // TODO à affecter à un Controlleur
    req.getConnection(function (err, conn) {
      if (err) return next("PB Connection");
      conn.query("UPDATE fleurs set ? WHERE id = ?", [data, fl_id], (err, rows) => {
        if (err) {
          console.log(err);
          return next("Mysql error ...");
        }
        res.send(rows);
      });
    });
  });

// DELETE
  flRoute.delete((req, res, next) => {
    let fl_id = req.params.fl_id;

    // TODO à affecter à un Controlleur
    req.getConnection(function (err, conn) {
      if (err) return next("PB Connection");
      conn.query("DELETE FROM fleurs WHERE id = ?", [fl_id], (err, rows) => {
        if (err) {
          console.log(err);
          return next("Mysql error ...");
        }
        res.send(rows);
      });
    });
  });
}
