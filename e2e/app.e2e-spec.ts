import { FleurboutikPage } from './app.po';

describe('fleurboutik App', () => {
  let page: FleurboutikPage;

  beforeEach(() => {
    page = new FleurboutikPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
