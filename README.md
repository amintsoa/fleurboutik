# Fleurboutik

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0. (Frontend)

Backend: Utilisation de ExpressJS (Node.js) avec une base de données MySQL.

## Installation 
Lancez `npm install`

## Lancement serveur
Executez la commande `node api.js`

## Lancement du frontend
Executez la commande `ng serve`
