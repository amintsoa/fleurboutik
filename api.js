let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let expressValidator = require('express-validator');
let cors = require('cors');
let connection = require('express-myconnection');
let mysql = require('mysql');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());
app.use(expressValidator());

/* DB local */
// let dbOptions = {
//   host: 'localhost',
//   user: 'root',
//   password: 'admin',
//   database: 'fleurboutik',
//   debug: false
// };

/* DB en ligne  */
let dbOptions = {
  host: 'db.dorianpharmatech.com',
  user: 'ranto',
  password: 'rantoranto',
  database: 'ranto',
  debug: false
};

app.use(
  connection(mysql, dbOptions, 'request')
);

// REST SERVICE
let router = express.Router();
require('./serveur/routes/fleurs.route')(router, app);
require('./serveur/routes/stat.route')(router, app);

app.use('/api', router);
app.listen(8081);
