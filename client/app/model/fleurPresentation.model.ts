/**
 * Projet: fleurboutik
 * Created by Ranto A. ANDRIANJAFY on 28/06/2017.
 * Contact: andrianjafy@gmail.com
 */

// "SELECT fleurs.id AS id, fleurs.nom AS nom, " +
// "fleurs.couleur AS idCouleur, couleur.nom AS couleur " +
// "FROM fleurs JOIN couleur ON fleurs.couleur = couleur.id " +
// "WHERE fleur.id = ?"

export interface FleurPresentation {
  id: number;
  nom: string;
  prix: number;
  couleur: number;
  nomCouleur: string;
}
