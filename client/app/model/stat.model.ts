/**
 * Projet: fleurboutik
 * Created by Ranto A. ANDRIANJAFY on 28/06/2017.
 * Contact: andrianjafy@gmail.com
 */

export interface StatModel {
  total: number;
  noir: number;
  blanc: number;
}
