import {Component, OnInit} from "@angular/core";
import {FleurService} from "../../fleur/fleur.service";
import {StatModel} from "../../model/stat.model";

@Component({
  selector: 'app-tableaudebord',
  templateUrl: './tableaudebord.component.html',
  styleUrls: ['./tableaudebord.component.css']
})
export class TableaudebordComponent implements OnInit {

  stat: StatModel = {
    total: 0,
    blanc: 0,
    noir: 0
  }

  constructor(private fleurService: FleurService) {
  }

  ngOnInit() {
    this.fleurService.getStat().subscribe(data => this.stat = data);
  }

}
