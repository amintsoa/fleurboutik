import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Gestion des stocks des fleurs.';

  constructor(private router: Router) {}

  activeRoute(routename: string): boolean{
    return this.router.url.indexOf(routename) > -1;
  }
}
