import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppComponent} from "./app.component";
import {FleurModule} from "./fleur/fleur.module";
import {RouterModule} from "@angular/router";
import {ROUTES} from "./app.routes";
import {HttpModule} from "@angular/http";
import { TableaudebordComponent } from './accueil/tableaudebord/tableaudebord.component';

@NgModule({
  declarations: [
    AppComponent,
    TableaudebordComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FleurModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
