import {Component, OnInit} from "@angular/core";
import {FleurService} from "../fleur.service";
import {FleurPresentation} from "../../model/fleurPresentation.model";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  fleurs: FleurPresentation[];

  constructor(private fleurService: FleurService) {
  }

  ngOnInit() {
    this.loadFleurs();
  }

  loadFleurs() {
    this.fleurService.getFleurs()
      .subscribe(fleurs => this.fleurs = fleurs);
  }

  delFleur(idFleur: number) {
    this.fleurService.deleteFleur(idFleur)
      .subscribe( () => {
        this.loadFleurs();
      });
  }

}
