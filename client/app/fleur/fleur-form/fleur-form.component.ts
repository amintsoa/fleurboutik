import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Fleur} from "../../model/fleur.model";
import {FleurService} from "../fleur.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-fleur-form',
  templateUrl: './fleur-form.component.html',
  styleUrls: ['./fleur-form.component.css']
})
export class FleurFormComponent implements OnInit {

  form: FormGroup;
  fleur: Fleur = {
    nom: '',
    prix: null,
    couleur: null
  };
  idFleur: number;

  formErrors = {
    'nom': '',
    'prix': '',
    'couleur': ''
  };

  validationMessages = {
    'nom': {
      'required': 'Le nom est obligatoire.',
      'maxlength': 'La taille du nom ne doit pas dépasser 100 caractères.',
      'minlength': 'La taille du nom doit être plus d\'un caractère.'
    },
    'prix': {
      'required': 'Le prix est obligatoire.',
      'min': 'Le prix doit être un nombre positif.'
    },
    'couleur': {
      'required': 'La couleur est obligatoire.'
    }
  };

  constructor(private fb: FormBuilder,
              private fleurService: FleurService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.buildForm();
    this.route.params.subscribe(params => {
      this.idFleur = params['id'];
      if (this.idFleur > 0) {
        this.fleurService.getFicheFleur(this.idFleur).subscribe(fleur => {
          this.fleur = fleur;
          this.buildForm();
        });
      }
    });
  }

  buildForm(): void {
    console.log(this.fleur);
    this.form = this.fb.group({
      nom: [this.fleur.nom, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(40)
      ]
      ],
      prix: [this.fleur.prix, [
        Validators.required,
        Validators.min(0)
      ]
      ],
      couleur: [this.fleur.couleur, [
        Validators.required
      ]]
    });
    this.form.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.form) {
      return;
    }
    const form = this.form;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  enregistreFleur(fleur): void {
    console.log(fleur);
    if (this.idFleur > 0) {
      this.fleurService.updateFleur(fleur, this.idFleur)
        .subscribe(result => {
          this.router.navigate(['fleur']);
        });
    } else {
      this.fleurService.newFleur(fleur)
        .subscribe(result => {
          this.router.navigate(['fleur']);
        });
    }
  }

}
