import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import {Fleur} from "../model/fleur.model";
import {FleurPresentation} from "../model/fleurPresentation.model";
import {StatModel} from "../model/stat.model";

@Injectable()
export class FleurService {

  private resourceFleurUrl = 'http://localhost:8081/api/fleur';
  private resourceStatUrl = 'http://localhost:8081/api/stat';

  constructor(private http: Http) {
  }

  /**
   * @returns Liste des fleurs
   */
  getFleurs(): Observable<FleurPresentation[]> {
    return this.http.get(`${this.resourceFleurUrl}`)
      .map((res: Response) => {
        return res.json();
      });
  }

  /**
   *
   * @param idFleur
   * @returns Fiche fleur
   */
  getFicheFleur(idFleur: number): Observable<FleurPresentation> {
    return this.http.get(`${this.resourceFleurUrl}/${idFleur}`)
      .map((res: Response) => {
        return res.json();
      });
  }

  newFleur(fleur: Fleur) {
    return this.http.post(`${this.resourceFleurUrl}`, fleur)
      .map((res: Response) => {
        return res.json();
      });
  }

  updateFleur(fleur: Fleur, idFleur: number) {
    return this.http.put(`${this.resourceFleurUrl}/${idFleur}`, fleur)
      .map((res: Response) => {
        return res.json();
      });
  }

  deleteFleur(idFleur: number) {
    return this.http.delete(`${this.resourceFleurUrl}/${idFleur}`)
      .map((res: Response) => {
        return res.json();
      });
  }

  getStat(): Observable<StatModel> {
    return this.http.get(`${this.resourceStatUrl}`)
      .map((res: Response) => {
        return res.json();
      });
  }
}
