import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MainComponent} from "./main/main.component";
import {FleurFormComponent} from "./fleur-form/fleur-form.component";
import {RouterModule} from "@angular/router";
import {FleurService} from "./fleur.service";
import {ReactiveFormsModule} from "@angular/forms";
import {ConfirmationPopoverModule} from "angular-confirmation-popover";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    })
  ],
  declarations: [MainComponent, FleurFormComponent],
  providers: [FleurService]
})
export class FleurModule {
}
