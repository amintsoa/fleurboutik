import {Routes} from "@angular/router";
import {MainComponent} from "./fleur/main/main.component";
import {FleurFormComponent} from "./fleur/fleur-form/fleur-form.component";
import {TableaudebordComponent} from "./accueil/tableaudebord/tableaudebord.component";

export const ROUTES: Routes = [
  {path: 'accueil', component: TableaudebordComponent},
  {path: 'fleur', component: MainComponent},
  {path: 'fleur/:id', component: FleurFormComponent},
  {path: '**', component: TableaudebordComponent},
];
